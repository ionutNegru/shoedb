$(document).ready(function (){

    $('#form').submit(function(e) {

        var name_shoe =  $('#nameShoe').val();
        var alias_shoe = $('#aliasShoe').val();
        var image_shoe = $('#imageShoe').val();

        var size_shoe = [];
        $.each($('input[id="size"]:checked'), function(){
            size_shoe.push($(this).val());
        });

        var sex_shoe = [];
        $.each($('input[id="sex"]:checked'), function(){
            sex_shoe.push($(this).val());
        });

        var color_shoe = [];
        $.each($('input[id="color"]:checked'), function(){
            color_shoe.push($(this).val());
        });

        $(".error").remove();

        if (image_shoe.length < 1) {
            $('#imageShoe').after('<span class="error" style="color: red">Acest camp este obligatoriu</span>');
            return false;
        } else if (name_shoe.length < 1) {
            $('#nameShoe').after('<span class="error" style="color: red">Acest camp este obligatoriu</span>');
            return false;
        } else if (name_shoe > 20) {
            $('#nameShoe').after('<span class="error" style="color: red">Campul contine mai mult decat 20 de caractere</span>');
            return false;
        } else if (alias_shoe.length < 1) {
            $('#aliasShoe').after('<span class="error" style="color: red">Acest camp este obligatoriu</span>');
            return false;
        } else if (alias_shoe.length > 20) {
            $('#nameShoe').after('<span class="error" style="color: red">Campul contine mai mult decat 20 de caractere</span>');
            return false;
        } else if (size_shoe.length < 1) {
            $('#sizeDiv').after('<span class="error" style="color: red">Acest camp este obligatoriu</span>');
            return false;
        } else if (sex_shoe.length < 1) {
            $('#sexDiv').after('<span class="error" style="color: red">Acest camp este obligatoriu</span>');
            return false;
        } else if (color_shoe.length < 1) {
            $('#colorDiv').after('<span class="error" style="color: red">Acest camp este obligatoriu</span>');
            return false;
        }
    });
});