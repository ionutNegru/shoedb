INSERT INTO type VALUES (1, 'Sport');
INSERT INTO type VALUES (2, 'Casual');

INSERT INTO color VALUES (1, 'Black');
INSERT INTO color VALUES (2, 'White');
INSERT INTO color VALUES (3, 'Red');
INSERT INTO color VALUES (4, 'Blue');
INSERT INTO color VALUES (5, 'Green');

INSERT INTO size VALUES (1, '38');
INSERT INTO size VALUES (2, '38.5');

INSERT INTO sex VALUES (1, 'Barbat');
INSERT INTO sex VALUES (2, 'Femeie');

INSERT INTO brand VALUES (1, 'USA', 'Nike');
INSERT INTO brand VALUES (2, 'Germany', 'Adidas');
INSERT INTO brand VALUES (3, 'USA', 'Jordan');

INSERT INTO price VALUES (1, '300');
INSERT INTO price VALUES (2, '350');
INSERT INTO price VALUES (3, '400');

INSERT INTO shoe VALUES (1, 'Air 450', 'ff', 'Air Maxs', 1, 1, 1);
INSERT INTO shoe VALUES (3, 'Superstar', 'sdsd', 'Adidas', 3, 1, 2);
INSERT INTO shoe VALUES (12, 'Air 250', 'll', 'Air Nax', 1, 1, 1);