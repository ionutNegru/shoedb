package com.example.shoeDB.controller;

import com.example.shoeDB.model.*;
import com.example.shoeDB.repository.ShoesRepository;
import com.example.shoeDB.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/editShoe")
public class EditShoeController {

    private List<Size> shoeSizeList = new ArrayList<>();

    private ShoesService shoesService;
    private ShoeTypeService shoeTypeService;
    private ShoePriceService shoePriceService;
    private ShoeBrandService shoeBrandService;
    private ShoeSizeService shoeSizeService;
    private ShoeSexService shoeSexService;
    private ShoeColorService shoeColorService;
    private ShoesRepository shoesRepository;

    @Autowired
    public EditShoeController(ShoesService shoesService, ShoeTypeService shoeTypeService,
                              ShoePriceService shoePriceService, ShoeBrandService shoeBrandService,
                              ShoeSizeService shoeSizeService, ShoeSexService shoeSexService, ShoeColorService shoeColorService,
                              ShoesRepository shoeRepository) {
        this.shoesService = shoesService;
        this.shoeTypeService = shoeTypeService;
        this.shoePriceService = shoePriceService;
        this.shoeBrandService = shoeBrandService;
        this.shoeSizeService = shoeSizeService;
        this.shoeSexService = shoeSexService;
        this.shoeColorService = shoeColorService;
        this.shoesRepository = shoeRepository;
    }

    @GetMapping
    public String getShoe(@RequestParam("idShoe") Long idShoe, Model model) {
        Shoe shoe = shoesService.findByIdShoe(idShoe);
        List<Shoe> shoeList = shoesRepository.findAll();
        List<Type> typeList = shoeTypeService.getAllTypes();
        List<Price> priceList = shoePriceService.getAllPrices();
        List<Size> sizeList = shoeSizeService.getAllSizes();
        List<Brand> brandList = shoeBrandService.getAllBrands();
        List<Sex> sexList = shoeSexService.getAllSexes();
        List<Color> colorList = shoeColorService.getAllColors();

        shoeSizeList = shoe.getSizeShoe();

        ArrayList<Shoe> availableShoes = new ArrayList<>(shoeList);
        ArrayList<Size> availableSizes = new ArrayList<>(sizeList);
        availableSizes.remove(shoe.getSizeShoe());

        for (Shoe itShoe : availableShoes) {
            for (Size itSize : itShoe.getSizeShoe()) {
                if (availableSizes.contains(itSize)) {
                    availableSizes.remove(itSize);
                }
            }
        }

        model.addAttribute("shoe", shoe);
        model.addAttribute("typeList", typeList);
        model.addAttribute("priceList", priceList);
        model.addAttribute("sizeList", availableSizes);
        model.addAttribute("brandList", brandList);
        model.addAttribute("sexList", sexList);
        model.addAttribute("colorList", colorList);

        return "edit";
    }

    @PostMapping(value = "updateShoe")
    public String editShoe(@ModelAttribute Shoe shoe, RedirectAttributes redirectAttributes) {
        List<Size> finalSizeList = shoeSizeList;

        if (shoe.getSizeShoe() != null) {
            for (Size size : shoe.getSizeShoe()) {
                finalSizeList.add(size);
            }
        }
        shoe.setSizeShoe(finalSizeList);
        shoesService.updateShoe(shoe);

        redirectAttributes.addAttribute("pageNumber", 0);
        return "redirect:/shoes";
    }
}
