package com.example.shoeDB.controller;


import com.example.shoeDB.model.*;
import com.example.shoeDB.repository.ShoesRepository;
import com.example.shoeDB.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/addShoe")
public class AddShoeController {

    private ShoesService shoesService;
    private ShoeTypeService shoeTypeService;
    private ShoePriceService shoePriceService;
    private ShoeBrandService shoeBrandService;
    private ShoeSizeService shoeSizeService;
    private ShoeSexService shoeSexService;
    private ShoeColorService shoeColorService;
    private ShoesRepository shoesRepository;

    @Autowired
    public AddShoeController(ShoesService shoesService, ShoeTypeService shoeTypeService,
                              ShoePriceService shoePriceService, ShoeBrandService shoeBrandService,
                              ShoeSizeService shoeSizeService, ShoeSexService shoeSexService, ShoeColorService shoeColorService,
                             ShoesRepository shoesRepository) {
        this.shoesService = shoesService;
        this.shoeTypeService = shoeTypeService;
        this.shoePriceService = shoePriceService;
        this.shoeBrandService = shoeBrandService;
        this.shoeSizeService = shoeSizeService;
        this.shoeSexService = shoeSexService;
        this.shoeColorService = shoeColorService;
        this.shoesRepository = shoesRepository;
    }

    @GetMapping
    public String showForm(Model model){

        List<Shoe> shoeList = shoesRepository.findAll();
        List<Type> typeList = shoeTypeService.getAllTypes();
        List<Price> priceList = shoePriceService.getAllPrices();
        List<Size> sizeList = shoeSizeService.getAllSizes();
        List<Brand> brandList = shoeBrandService.getAllBrands();
        List<Sex> sexList = shoeSexService.getAllSexes();
        List<Color> colorList = shoeColorService.getAllColors();


        ArrayList<Shoe> availableShoes = new ArrayList<>(shoeList);
        ArrayList<Size> availableSizes = new ArrayList<>(sizeList);

        for(Shoe itShoe : availableShoes){
            for(Size itSize: itShoe.getSizeShoe()){
                if(availableSizes.contains(itSize)){
                    availableSizes.remove(itSize);
                }
            }
        }

        model.addAttribute("shoe", new Shoe());
        model.addAttribute("typeList",typeList);
        model.addAttribute("priceList",priceList);
        model.addAttribute("sizeList",availableSizes);
        model.addAttribute("brandList",brandList);
        model.addAttribute("sexList",sexList);
        model.addAttribute("colorList",colorList);


        return "add";
    }

    @PostMapping(value = "saveShoe")
    public String saveShoe(@ModelAttribute("shoe") Shoe shoe,  RedirectAttributes redirectAttributes){
        redirectAttributes.addAttribute("pageNumber",0);
        shoesService.updateShoe(shoe);
        return "redirect:/shoes";
    }
}
