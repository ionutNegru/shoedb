package com.example.shoeDB.controller;

import com.example.shoeDB.model.Shoe;
import com.example.shoeDB.service.ShoesService;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/shoes")
public class ShoesController {

    private ShoesService shoesService;

    public ShoesController(ShoesService shoesService){
        this.shoesService = shoesService;
    }

    @GetMapping()
    public String getShoes(Model model, @RequestParam int pageNumber){
        Page<Shoe> shoeList = shoesService.getAllShoes(pageNumber);
        model.addAttribute("shoeList",shoeList);
        model.addAttribute("currentPage",pageNumber);
        return "home";
    }

    @GetMapping(value = "editShoe")
    public String editShoe(@RequestParam Long idShoe, @NotNull RedirectAttributes redirectAttributes){
        //ModelMap pentru redirectionare si in semnatura -> acces cu PathVariable("atribut")
        //redirect.addAttribute -> acces cu RequestParame("atribut")
        //redirect.addFlashAttribute -> acces cu un ModelMap, fara a fi nevoie de @
        //                           -> acces cu ModelMap(obiect) daca transmit obiect

        redirectAttributes.addAttribute("idShoe",idShoe);
        return "redirect:/editShoe";
    }

    @GetMapping(value = "deleteShoe")
    public String deleteShoe(@RequestParam Long idShoe, RedirectAttributes redirectAttributes){
        shoesService.deleteShoe(idShoe);
        redirectAttributes.addAttribute("pageNumber",0);
        return "redirect:/shoes";
    }

    @GetMapping(value = "addShoe")
    public String addShoe() {
        return "redirect:/addShoe";
    }

}
