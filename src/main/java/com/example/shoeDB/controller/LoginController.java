package com.example.shoeDB.controller;

import com.example.shoeDB.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    @GetMapping
    public String showLoginForm(Model model){
        User user = new User();
        model.addAttribute("user",user);
        return "login";
    }
}
