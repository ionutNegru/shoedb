package com.example.shoeDB.repository;

import com.example.shoeDB.model.Type;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ShoeTypeRepository extends PagingAndSortingRepository<Type, Long> {

}
