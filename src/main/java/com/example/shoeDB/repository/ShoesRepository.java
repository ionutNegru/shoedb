package com.example.shoeDB.repository;

import com.example.shoeDB.model.Shoe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoesRepository extends JpaRepository<Shoe, Long> {

    Shoe findByIdShoe(Long Id);
}
