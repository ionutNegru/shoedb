package com.example.shoeDB.repository;

import com.example.shoeDB.model.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShoeBrandRepository extends JpaRepository<Brand, Long> {

    @Query("SELECT b.nameBrand FROM Brand b")
    List<String> findAllBrandNames();
}
