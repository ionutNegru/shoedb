package com.example.shoeDB.repository;

import com.example.shoeDB.model.Size;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoeSizeRepository extends JpaRepository<Size, Long> {
}
