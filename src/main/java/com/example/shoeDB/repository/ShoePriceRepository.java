package com.example.shoeDB.repository;

import com.example.shoeDB.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ShoePriceRepository extends JpaRepository<Price, Long> {

}
