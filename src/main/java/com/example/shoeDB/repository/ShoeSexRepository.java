package com.example.shoeDB.repository;

import com.example.shoeDB.model.Sex;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoeSexRepository extends JpaRepository<Sex, Long> {
}
