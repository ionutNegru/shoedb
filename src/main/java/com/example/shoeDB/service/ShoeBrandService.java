package com.example.shoeDB.service;

import com.example.shoeDB.model.Brand;

import java.util.List;

public interface ShoeBrandService {

     List<Brand> getAllBrands();

     List<String> getAllBrandNames();
}
