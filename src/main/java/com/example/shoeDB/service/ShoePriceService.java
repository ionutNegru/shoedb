package com.example.shoeDB.service;


import com.example.shoeDB.model.Price;

import java.util.List;

public interface ShoePriceService {

     List<Price> getAllPrices();
}
