package com.example.shoeDB.service;

import com.example.shoeDB.model.Size;

import java.util.List;

public interface ShoeSizeService {

     List<Size> getAllSizes();
}
