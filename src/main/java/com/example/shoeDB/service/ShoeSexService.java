package com.example.shoeDB.service;

import com.example.shoeDB.model.Sex;

import java.util.List;

public interface ShoeSexService {

     List<Sex> getAllSexes();
}
