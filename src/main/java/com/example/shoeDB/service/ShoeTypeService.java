package com.example.shoeDB.service;

import com.example.shoeDB.model.Type;

import java.util.List;

public interface ShoeTypeService {

     List<Type> getAllTypes();
}
