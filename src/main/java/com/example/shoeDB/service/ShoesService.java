package com.example.shoeDB.service;

import com.example.shoeDB.model.Shoe;
import org.springframework.data.domain.Page;


public interface ShoesService {

     Page<Shoe> getAllShoes(int page);

     Shoe findByIdShoe(Long Id);

     Shoe updateShoe(Shoe shoe);

     void deleteShoe(Long idShoe);
}
