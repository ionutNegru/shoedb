package com.example.shoeDB.service;

import com.example.shoeDB.model.Color;

import java.util.List;

public interface ShoeColorService {

     List<Color> getAllColors();
}
