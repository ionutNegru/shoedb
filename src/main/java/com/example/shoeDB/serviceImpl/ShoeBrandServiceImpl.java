package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Brand;
import com.example.shoeDB.repository.ShoeBrandRepository;
import com.example.shoeDB.service.ShoeBrandService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoeBrandServiceImpl implements ShoeBrandService {

    private ShoeBrandRepository shoeBrandRepository;

    public ShoeBrandServiceImpl(ShoeBrandRepository shoeBrandRepository){
        this.shoeBrandRepository = shoeBrandRepository;
    }

    @Override
    public List<Brand> getAllBrands() {
        return shoeBrandRepository.findAll();
    }

    @Override
    public List<String> getAllBrandNames() {
        return shoeBrandRepository.findAllBrandNames();
    }
}
