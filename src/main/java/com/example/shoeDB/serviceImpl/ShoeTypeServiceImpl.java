package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Type;
import com.example.shoeDB.repository.ShoeTypeRepository;
import com.example.shoeDB.service.ShoeTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoeTypeServiceImpl implements ShoeTypeService {

    private ShoeTypeRepository shoeTypeRepository;

    public ShoeTypeServiceImpl(ShoeTypeRepository shoeTypeRepository){
        this.shoeTypeRepository = shoeTypeRepository;
    }

    @Override
    public List<Type> getAllTypes() {
        return (List<Type>) shoeTypeRepository.findAll();
    }
}
