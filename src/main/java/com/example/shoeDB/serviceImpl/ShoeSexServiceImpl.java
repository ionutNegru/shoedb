package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Sex;
import com.example.shoeDB.repository.ShoeSexRepository;
import com.example.shoeDB.service.ShoeSexService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoeSexServiceImpl implements ShoeSexService {

    private ShoeSexRepository shoeSexRepository;

    public ShoeSexServiceImpl(ShoeSexRepository shoeSexRepository){
        this.shoeSexRepository = shoeSexRepository;
    }

    @Override
    public List<Sex> getAllSexes() {
        return shoeSexRepository.findAll();
    }
}
