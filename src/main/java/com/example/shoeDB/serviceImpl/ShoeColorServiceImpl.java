package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Color;
import com.example.shoeDB.repository.ShoeColorRepository;
import com.example.shoeDB.service.ShoeColorService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoeColorServiceImpl implements ShoeColorService {

    private ShoeColorRepository shoeColorRepository;

    public ShoeColorServiceImpl(ShoeColorRepository shoeColorRepository){
        this.shoeColorRepository = shoeColorRepository;
    }

    @Override
    public List<Color> getAllColors() {
        return shoeColorRepository.findAll();
    }
}
