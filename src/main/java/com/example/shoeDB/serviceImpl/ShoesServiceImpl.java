package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Shoe;
import com.example.shoeDB.repository.ShoesRepository;
import com.example.shoeDB.service.ShoesService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ShoesServiceImpl implements ShoesService {

    private ShoesRepository shoesRepository;

    public ShoesServiceImpl(ShoesRepository shoesRepository){
        this.shoesRepository = shoesRepository;
    }

    @Override
    public Page<Shoe> getAllShoes(int page) {
        return shoesRepository.findAll(PageRequest.of(page, 4, Sort.by("idShoe").ascending()));
    }

    @Override
    public Shoe findByIdShoe(Long Id) {
        return shoesRepository.findByIdShoe(Id);
    }

    @Override
    @Transactional
    public Shoe updateShoe(Shoe shoe) {
        return shoesRepository.save(shoe);
    }

    @Override
    @Transactional
    public void deleteShoe(Long idShoe) {
        shoesRepository.deleteById(idShoe);
    }

}
