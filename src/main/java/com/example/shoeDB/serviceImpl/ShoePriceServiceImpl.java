package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Price;
import com.example.shoeDB.repository.ShoePriceRepository;
import com.example.shoeDB.service.ShoePriceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoePriceServiceImpl implements ShoePriceService {

    private ShoePriceRepository shoePriceRepository;

    public ShoePriceServiceImpl (ShoePriceRepository shoePriceRepository) {
        this.shoePriceRepository = shoePriceRepository;
    }

    @Override
    public List<Price> getAllPrices() {
        return shoePriceRepository.findAll();
    }
}
