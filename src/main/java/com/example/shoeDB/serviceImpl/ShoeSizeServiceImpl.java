package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Size;
import com.example.shoeDB.repository.ShoeSizeRepository;
import com.example.shoeDB.service.ShoeSizeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoeSizeServiceImpl implements ShoeSizeService {

    private ShoeSizeRepository shoeSizeRepository;

    public ShoeSizeServiceImpl(ShoeSizeRepository shoeSizeRepository){
        this.shoeSizeRepository = shoeSizeRepository;
    }

    @Override
    public List<Size> getAllSizes() {
        return shoeSizeRepository.findAll();
    }
}
