package com.example.shoeDB.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Sex {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long idSex;
    private String nameSex;

    @Override
    public String toString() {
        return nameSex;
    }

    public Long getIdSex() {
        return idSex;
    }

    public void setIdSex(Long idSex) {
        this.idSex = idSex;
    }

    public String getNameSex() {
        return nameSex;
    }

    public void setNameSex(String nameSex) {
        this.nameSex = nameSex;
    }
}
