package com.example.shoeDB.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Brand {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long idBrand;

    private String nameBrand;

    private String countryBrand;

    @Override
    public String toString() {
        return nameBrand;
    }

    public Long getIdBrand() {
        return idBrand;
    }

    public void setIdBrand(Long idBrand) {
        this.idBrand = idBrand;
    }

    public String getNameBrand() {
        return nameBrand;
    }

    public void setNameBrand(String nameBrand) {
        this.nameBrand = nameBrand;
    }

    public String getCountryBrand() {
        return countryBrand;
    }

    public void setCountryBrand(String countryBrand) {
        this.countryBrand = countryBrand;
    }
}
