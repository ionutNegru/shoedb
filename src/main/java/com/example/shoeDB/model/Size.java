package com.example.shoeDB.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Size {

    @Id
    private Long idSize;

    private String noSize;

    @Override
    public String toString() {
        return noSize;
    }

    public Long getIdSize() {
        return idSize;
    }

    public void setIdSize(Long idSize) {
        this.idSize = idSize;
    }

    public String getNoSize() {
        return noSize;
    }

    public void setNoSize(String noSize) {
        this.noSize = noSize;
    }

}
