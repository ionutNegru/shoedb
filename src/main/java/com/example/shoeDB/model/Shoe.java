package com.example.shoeDB.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Shoe {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long idShoe;

    private String nameShoe;

    private String aliasShoe;

    private String image;

    @OneToOne
    private Type typeShoe;

    @OneToMany
    private List<com.example.shoeDB.model.Size> sizeShoe;

    @ManyToMany
    private List<Color> colorShoe;

    @ManyToMany
    private List<Sex> sexShoe;

    @ManyToOne
    private Brand brandShoe;

    @ManyToOne//(cascade = {CascadeType.MERGE}) // propagarea modificarilor si catre tabela Price
    private Price priceShoe;

    @Override
    public String toString() {
        return "Adidasul : nume: " + nameShoe + " alias: " + aliasShoe + " tip: " + typeShoe.toString() + " marimi: "
                + sizeShoe.toString() + " culori: " + colorShoe.toString() + " sexe:" + sexShoe.toString() + " brand: " + brandShoe.toString() + " pret: "
                + priceShoe.toString();
    }

    public Long getIdShoe() {
        return idShoe;
    }

    public void setIdShoe(Long idShoe) {
        this.idShoe = idShoe;
    }

    public String getNameShoe() {
        return nameShoe;
    }

    public void setNameShoe(String nameShoe) {
        this.nameShoe = nameShoe;
    }

    public String getAliasShoe() {
        return aliasShoe;
    }

    public void setAliasShoe(String aliasShoe) {
        this.aliasShoe = aliasShoe;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Type getTypeShoe() {
        return typeShoe;
    }

    public void setTypeShoe(Type typeShoe) {
        this.typeShoe = typeShoe;
    }

    public List<com.example.shoeDB.model.Size> getSizeShoe() {
        return sizeShoe;
    }

    public void setSizeShoe(List<com.example.shoeDB.model.Size> sizeShoe) {
        this.sizeShoe = sizeShoe;
    }

    public List<Color> getColorShoe() {
        return colorShoe;
    }

    public void setColorShoe(List<Color> colorShoe) {
        this.colorShoe = colorShoe;
    }

    public List<Sex> getSexShoe() {
        return sexShoe;
    }

    public void setSexShoe(List<Sex> sexShoe) {
        this.sexShoe = sexShoe;
    }

    public Brand getBrandShoe() {
        return brandShoe;
    }

    public void setBrandShoe(Brand brandShoe) {
        this.brandShoe = brandShoe;
    }

    public Price getPriceShoe() {
        return priceShoe;
    }

    public void setPriceShoe(Price priceShoe) {
        this.priceShoe = priceShoe;
    }
}
