package com.example.shoeDB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@SpringBootApplication
@EntityScan("com/example/shoeDB/model")
public class ShoeDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoeDbApplication.class, args);
	}

	@GetMapping("/")
	public ModelAndView toIndex(){
		return new ModelAndView("redirect:/login");
	}

}
