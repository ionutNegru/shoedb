package com.example.shoeDB.aspect;

import com.example.shoeDB.model.Shoe;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class LoggerAspect {

    private static final Logger logger = Logger.getLogger("ShoeAspect");

    @Pointcut("execution(* com.example.shoeDB.service.*.getAll*(..))")
    public void serviceGet() {}

    @Pointcut("execution(* com.example.shoeDB.service.ShoesService.findByIdShoe(..))")
    public void serviceGetById() {}

    private static long getByIdCount = 0;

    @Pointcut("execution(* com.example.shoeDB.service.ShoesService.deleteShoe(..))")
    public void serviceDelete() {}

    private static long deleteCount = 0;

    @Pointcut("execution(* com.example.shoeDB.service.ShoesService.updateShoe(..))")
    public void serviceUpdate() {}

    private static long updateCount = 0;

    @Before("serviceGet()")
    public void beforeGet(JoinPoint joinPoint) {
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        logger.info(" ---> Metoda " + className + "." + methodName
                + " se va apela");
    }

    @After("serviceGet()")
    public void afterGet(JoinPoint joinPoint) {
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        logger.info(" ---> Metoda " + className + "." + methodName
                + " a fost apelata");
    }

    @Before("serviceGetById()")
    public void beforeGetById(JoinPoint joinPoint) {
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        Long idShoe = (Long)args[0];

        logger.info(" ---> Metoda " + className + "." + methodName
                + " se va apela cu parametrul " + idShoe.toString());
    }

    @After("serviceGetById()")
    public void afterGetById(JoinPoint joinPoint){
        getByIdCount++;
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        Long idShoe = (Long)args[0];

        logger.info(" ---> Metoda " + className + "." + methodName
                + " se va apela cu parametrul " + idShoe.toString() + " de " + getByIdCount + " ori");
    }

    @Before("serviceUpdate()")
    public void beforeUpdate(JoinPoint joinPoint) {
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        Shoe shoe = (Shoe) args[0];

        logger.info(" ---> Metoda " + className + "." + methodName
                + " se va apela cu parametrul " + shoe.toString());
    }

    @After("serviceUpdate()")
    public void afterUpdate(JoinPoint joinPoint) {
        updateCount++;
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        Shoe shoe = (Shoe) args[0];

        logger.info(" ---> Metoda " + className + "." + methodName
                + " se va apela cu parametrul " + shoe.toString() + " de " + updateCount + " ori");
    }

    @Before("serviceDelete()")
    public void beforeDelete(JoinPoint joinPoint) {
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        Long idShoe = (Long)args[0];

        logger.info(" ---> Metoda " + className + "." + methodName
                + " se va apela cu parametrul " + idShoe.toString());
    }

    @After("serviceDelete()")
    public void afterDelete(JoinPoint joinPoint){
        deleteCount++;
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        Long idShoe = (Long)args[0];

        logger.info(" ---> Metoda " + className + "." + methodName
                + " se va apela cu parametrul " + idShoe.toString() + " de " + deleteCount + " ori");
    }
}
