package com.example.shoeDB.repository;

import com.example.shoeDB.model.Price;
import com.example.shoeDB.model.Size;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ShoeSizeRepositoryTest {

    @Autowired
    private  ShoeSizeRepository shoeSizeRepository;

    @BeforeEach
     void setUp(){
        assertNotNull(shoeSizeRepository);
    }

    @Test
    void findAll() {
        List<Size> listPrices = shoeSizeRepository.findAll();
        assertEquals(2, listPrices.size());
    }

}