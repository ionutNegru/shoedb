package com.example.shoeDB.repository;

import com.example.shoeDB.model.Price;
import com.example.shoeDB.model.Sex;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ShoeSexRepositoryTest {

    @Autowired
    private  ShoeSexRepository shoeSexRepository;

    @BeforeEach
     void setUp(){
        assertNotNull(shoeSexRepository);
    }

    @Test
    void findAll() {
        List<Sex> listSexes = shoeSexRepository.findAll();
        assertEquals(2, listSexes.size());
    }

}