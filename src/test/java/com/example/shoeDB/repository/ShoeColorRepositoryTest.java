package com.example.shoeDB.repository;

import com.example.shoeDB.model.Color;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ShoeColorRepositoryTest {

    @Autowired
    private  ShoeColorRepository shoeColorRepository;

    @BeforeEach
     void setUp() {
        assertNotNull(shoeColorRepository);
    }

    @Test
    void findAll() {
        List<Color> listColors = shoeColorRepository.findAll();
        assertEquals(5, listColors.size());
    }
}