package com.example.shoeDB.repository;

import com.example.shoeDB.model.Shoe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ShoesRepositoryTest {

    @Autowired
    private  ShoesRepository shoesRepository;

    @BeforeEach
     void setUp(){
        assertNotNull(shoesRepository);
    }

    @Test
    void findAll() {
        Page<Shoe> shoeList = shoesRepository.findAll(PageRequest.of(0, 4, Sort.by("idShoe").ascending()));
        assertEquals(3, shoeList.getTotalElements());
    }

    @Test
    void save() {
        Shoe shoe = new Shoe();
        shoe.setIdShoe(2L);
        shoe.setNameShoe("TestShoe");

        Shoe savedShoe = shoesRepository.save(shoe);
        assertEquals(shoe.getNameShoe(), savedShoe.getNameShoe());
    }

    @Test
    void update() {
        Shoe shoe = shoesRepository.getOne(12L);
        shoe.setNameShoe("UpdatedShoe");

        Shoe savedShoe = shoesRepository.save(shoe);
        assertEquals(shoe.getIdShoe(), savedShoe.getIdShoe());
        assertNotEquals("Air 250", savedShoe.getNameShoe());
    }

    @Test
    void delete() {
        Shoe shoe = shoesRepository.getOne(12L);

        shoesRepository.delete(shoe);
        List<Shoe> list = shoesRepository.findAll();
        assertEquals(2,list.size());
    }

}