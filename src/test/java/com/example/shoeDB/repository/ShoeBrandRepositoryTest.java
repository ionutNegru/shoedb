package com.example.shoeDB.repository;

import com.example.shoeDB.model.Brand;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ShoeBrandRepositoryTest {

    @Autowired
    private ShoeBrandRepository shoeBrandRepository;

    @BeforeEach
     void setUp(){
        assertNotNull(shoeBrandRepository);
    }

    @Test
    void findAllBrandNames() {
        List<String> listBrands = shoeBrandRepository.findAllBrandNames();
        assertEquals(3, listBrands.size());
    }

    @Test
    void findAll() {
        List<Brand> list = shoeBrandRepository.findAll();
        assertEquals(3, list.size());
    }
}