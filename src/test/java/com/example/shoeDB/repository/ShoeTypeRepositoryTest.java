package com.example.shoeDB.repository;

import com.example.shoeDB.model.Type;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ShoeTypeRepositoryTest {

    @Autowired
    private ShoeTypeRepository shoeTypeRepository;

    @BeforeEach
     void setUp() {
        assertNotNull(shoeTypeRepository);
    }

    @Test
    void findAll() {
        List<Type> typeList = (List<Type>) shoeTypeRepository.findAll();
        assertEquals(1, typeList.size());
    }

}