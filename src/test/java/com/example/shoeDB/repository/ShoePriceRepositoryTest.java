package com.example.shoeDB.repository;

import com.example.shoeDB.model.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ShoePriceRepositoryTest {

    @Autowired
    private  ShoePriceRepository shoePriceRepository;

    @BeforeEach
     void setUp(){
        assertNotNull(shoePriceRepository);
    }

    @Test
    void findAll() {
        List<Price> listPrices = shoePriceRepository.findAll();
        assertEquals(3, listPrices.size());
    }

}