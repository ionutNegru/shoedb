package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Type;
import com.example.shoeDB.repository.ShoeTypeRepository;
import com.example.shoeDB.service.ShoeTypeService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShoeTypeServiceImplTest {

    @InjectMocks
    private ShoeTypeServiceImpl shoeTypeService;

    @Mock
    private ShoeTypeRepository shoeTypeRepository;

    private static List<Type> mockTypeList;

    @BeforeAll
    static void setUp() {
        mockTypeList = new ArrayList<>();

        Type mockType = new Type();
        mockType.setIdType(1L);

        mockTypeList.add(mockType);
    }

    @Test
    void getAllTypes() {
        when(shoeTypeRepository.findAll()).thenReturn(mockTypeList);
        List<Type> resultTypeList = shoeTypeService.getAllTypes();

        assertEquals(1,resultTypeList.size());
        verify(shoeTypeRepository, times(1)).findAll();
    }
}