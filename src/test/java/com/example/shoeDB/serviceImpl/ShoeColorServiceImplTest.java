package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Color;
import com.example.shoeDB.repository.ShoeColorRepository;
import com.example.shoeDB.service.ShoeColorService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShoeColorServiceImplTest {

    @InjectMocks
    private ShoeColorServiceImpl shoeColorService;

    @Mock
    private ShoeColorRepository shoeColorRepository;

    private static List<Color> mockColorList;

    @BeforeAll
    static void setUp() {
        mockColorList = new ArrayList<>();

        Color mockColor = new Color();
        mockColor.setIdColor(1L);

        mockColorList.add(mockColor);
    }

    @Test
    void getAllColors() {
        when(shoeColorRepository.findAll()).thenReturn(mockColorList);
        List<Color> resultColorList = shoeColorService.getAllColors();

        assertEquals(1,resultColorList.size());
        verify(shoeColorRepository, times(1)).findAll();
    }
}