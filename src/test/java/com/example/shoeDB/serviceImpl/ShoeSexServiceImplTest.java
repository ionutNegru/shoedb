package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Sex;
import com.example.shoeDB.model.Shoe;
import com.example.shoeDB.repository.ShoeSexRepository;
import com.example.shoeDB.service.ShoeSexService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShoeSexServiceImplTest {

    @InjectMocks
    private ShoeSexServiceImpl shoeSexService;

    @Mock
    private ShoeSexRepository shoeSexRepository;

    private static List<Sex> mockSexList;

    @BeforeAll
    static void setUp() {
        mockSexList = new ArrayList<>();

        Sex mockSex = new Sex();
        mockSex.setIdSex(1L);

        mockSexList.add(mockSex);
    }

    @Test
    void getAllSexes() {
        when(shoeSexRepository.findAll()).thenReturn(mockSexList);
        List<Sex> resultSexList = shoeSexService.getAllSexes();

        assertEquals(1, resultSexList.size());
        verify(shoeSexRepository, times(1)).findAll();
    }
}