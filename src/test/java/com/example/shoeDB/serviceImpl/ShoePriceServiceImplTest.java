package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Price;
import com.example.shoeDB.repository.ShoePriceRepository;
import com.example.shoeDB.service.ShoePriceService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShoePriceServiceImplTest {

    @InjectMocks
    private ShoePriceServiceImpl shoePriceService;

    @Mock
    private ShoePriceRepository shoePriceRepository;

    private static List<Price> mockPriceList;

    @BeforeAll
    static void setUp() {
        mockPriceList = new ArrayList<>();

        Price mockPrice = new Price();
        mockPrice.setIdPrice(1L);

        mockPriceList.add(mockPrice);
    }

    @Test
    void getAllPrices() {
        when(shoePriceRepository.findAll()).thenReturn(mockPriceList);
        List<Price> resultPriceList = shoePriceService.getAllPrices();

        assertEquals(1, resultPriceList.size());
        verify(shoePriceRepository, times(1)).findAll();
    }
}