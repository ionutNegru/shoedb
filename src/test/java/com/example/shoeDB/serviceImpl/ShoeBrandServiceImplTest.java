package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Brand;
import com.example.shoeDB.repository.ShoeBrandRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShoeBrandServiceImplTest {

    @InjectMocks
    private ShoeBrandServiceImpl shoeBrandService;

    @Mock
    private ShoeBrandRepository shoeBrandRepository;

    private static List<Brand> mockBrandList;


    @BeforeAll
    static void setUp() {
        mockBrandList = new ArrayList<>();

        Brand mockBrand = new Brand();
        mockBrand.setIdBrand(1L);
        mockBrand.setNameBrand("Nike");

        mockBrandList.add(mockBrand);
    }

    @Test
    void getAllBrands() {
        when(shoeBrandRepository.findAll()).thenReturn(mockBrandList);
        List<Brand> resultBrandList = shoeBrandService.getAllBrands();

        assertEquals(1, resultBrandList.size());
        verify(shoeBrandRepository, times(1)).findAll();
    }

    @Test
    void getAllBrandNames() {
        List<String> mockNames = new ArrayList<>();
        for (Brand brand : mockBrandList) {
            mockNames.add(brand.getNameBrand());
        }

        when(shoeBrandRepository.findAllBrandNames()).thenReturn(mockNames);
        List<String> resultNameList = shoeBrandService.getAllBrandNames();

        assertEquals("Nike",resultNameList.get(0));
        verify(shoeBrandRepository, times(1)).findAllBrandNames();
    }
}