package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Shoe;
import com.example.shoeDB.repository.ShoesRepository;
import com.example.shoeDB.service.ShoesService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShoesServiceImplTest {

    @InjectMocks
    private ShoesServiceImpl shoesService;

    @Mock
    private ShoesRepository shoesRepository;

    private static List<Shoe> mockShoeList;

    private static Page<Shoe> mockShoePage;

    private static Shoe mockShoe;

    @BeforeAll
    static void setUp() {
        mockShoeList = new ArrayList<>();

        mockShoe = new Shoe();
        mockShoe.setIdShoe(1L);
        mockShoe.setNameShoe("Name");

        mockShoeList.add(mockShoe);
        mockShoePage = Page.empty();
        mockShoePage = new PageImpl<>(mockShoeList);
    }

    @Test
    void getAllShoes() {
        when(shoesRepository.findAll(PageRequest.of(0, 4, Sort.by("idShoe").ascending()))).thenReturn(mockShoePage);
        Page<Shoe> resultShoePage = shoesService.getAllShoes(0);

        assertEquals(1L,resultShoePage.getTotalElements());
        verify(shoesRepository, times(1)).findAll(PageRequest.of(0, 4, Sort.by("idShoe").ascending()));
    }

    @Test
    void findShoeByShoeId() {
        when(shoesRepository.findByIdShoe(1L)).thenReturn(mockShoe);
        Shoe resultShoe = shoesService.findByIdShoe(1L);

        assertEquals(mockShoe.getIdShoe(),resultShoe.getIdShoe());
        verify(shoesRepository, times(1)).findByIdShoe(1L);
    }

    @Test
    void updateShoe() {
        Shoe mockShoeUpdated = new Shoe();
        mockShoeUpdated.setIdShoe(1L);
        mockShoeUpdated.setNameShoe("UpdatedName");

        when(shoesRepository.save(mockShoeUpdated)).thenReturn(mockShoeUpdated);
        Shoe resultShoe = shoesService.updateShoe(mockShoeUpdated);

        assertEquals(mockShoeUpdated.getIdShoe(), resultShoe.getIdShoe());
        assertNotEquals(mockShoe.getNameShoe(), resultShoe.getNameShoe());
        verify(shoesRepository, times(1)).save(mockShoeUpdated);
    }

    @Test
    void deleteShoe() {
        when(shoesRepository.findById(mockShoe.getIdShoe())).thenReturn(null);
        shoesRepository.delete(mockShoe);

        assertEquals(null, shoesRepository.findById(1L));
        verify(shoesRepository, times(1)).delete(mockShoe);
        verify(shoesRepository, times(1)).findById(1L);
    }
}