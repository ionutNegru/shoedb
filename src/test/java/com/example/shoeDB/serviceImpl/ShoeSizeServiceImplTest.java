package com.example.shoeDB.serviceImpl;

import com.example.shoeDB.model.Size;
import com.example.shoeDB.repository.ShoeSizeRepository;
import com.example.shoeDB.service.ShoeSizeService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShoeSizeServiceImplTest {

    @InjectMocks
    private ShoeSizeServiceImpl shoeSizeService;

    @Mock
    private ShoeSizeRepository shoeSizeRepository;

    private static List<Size> mockSizeList;

    @BeforeAll
    static void setUp() {
        mockSizeList = new ArrayList<>();

        Size mockSize = new Size();
        mockSize.setIdSize(1L);

        mockSizeList.add(mockSize);
    }

    @Test
    void getAllSizes() {
        when(shoeSizeRepository.findAll()).thenReturn(mockSizeList);
        List<Size> resultSizeList = shoeSizeService.getAllSizes();

        assertEquals(1,resultSizeList.size());
        verify(shoeSizeRepository, times(1)).findAll();
    }
}