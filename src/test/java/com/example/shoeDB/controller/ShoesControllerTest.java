package com.example.shoeDB.controller;

import com.example.shoeDB.model.Shoe;
import com.example.shoeDB.service.ShoesService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class ShoesControllerTest {

    @InjectMocks
    private ShoesController shoesController;

    @Mock
    private ShoesService shoesService;

    @Captor
    private ArgumentCaptor<Page<Shoe>> captor;

    @Captor
    private ArgumentCaptor<Long> idCaptor;

    @Mock
    private Model model;

    @Mock
    private RedirectAttributes redirectAttributes;

    private static Shoe mockShoe;

    private static List<Shoe> mockShoeList;

    private static Page<Shoe> mockShoePage;

    @BeforeAll
    static void setUp() {
        mockShoeList = new ArrayList<>();

        mockShoe = new Shoe();
        mockShoe.setIdShoe(1L);

        mockShoeList.add(mockShoe);
        mockShoePage = Page.empty();
        mockShoePage = new PageImpl<>(mockShoeList);
    }

    @Test
    void getShoes() throws Exception {
        when(shoesService.getAllShoes(0)).thenReturn(mockShoePage);

        String viewName = shoesController.getShoes(model, 0);
        assertEquals("home",viewName);

        verify(model, times(1))
                .addAttribute(eq("shoeList"), captor.capture());
        Page<Shoe> listArgument = captor.getValue();
        assertEquals(mockShoeList.size(), listArgument.getTotalElements());

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(shoesController).build();
        mockMvc.perform(
                get("/shoes").param("pageNumber","0"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("home"));
    }

    @Test
    void editShoe() throws Exception {
        String viewName = shoesController.editShoe(mockShoe.getIdShoe(), redirectAttributes);
        assertEquals("redirect:/editShoe",viewName);

        verify(redirectAttributes, times(1))
                .addAttribute(eq("idShoe"),idCaptor.capture());

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(shoesController).build();
        mockMvc.perform(
                get("/shoes/editShoe").param("idShoe",mockShoe.getIdShoe().toString()))
                .andExpect(redirectedUrl("/editShoe?idShoe=1"));
    }

    @Test
    void deleteShoe() throws Exception {
        String viewName = shoesController.deleteShoe(mockShoe.getIdShoe(),redirectAttributes);
        assertEquals("redirect:/shoes",viewName);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(shoesController).build();
        mockMvc.perform(
                get("/shoes/deleteShoe").param("idShoe",mockShoe.getIdShoe().toString()))
                .andExpect(redirectedUrl("/shoes?pageNumber=0"));

    }

    @Test
    void addShoe() throws Exception {
        String viewName = shoesController.addShoe();
        assertEquals("redirect:/addShoe",viewName);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(shoesController).build();
        mockMvc.perform(
                get("/shoes/addShoe"))
                .andExpect(redirectedUrl("/addShoe"));
    }
}