package com.example.shoeDB.controller;

import com.example.shoeDB.model.*;
import com.example.shoeDB.repository.ShoesRepository;
import com.example.shoeDB.service.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class AddShoeControllerTest {

    @InjectMocks
    private AddShoeController addShoeController;

    //    @MockBean
    @Mock
    private  ShoesService shoesService;

    @Mock
    private  ShoeTypeService shoeTypeService;

    @Mock
    private  ShoePriceService shoePriceService;

    @Mock
    private  ShoeBrandService shoeBrandService;

    @Mock
    private  ShoeSizeService shoeSizeService;

    @Mock
    private  ShoeSexService shoeSexService;

    @Mock
    private  ShoeColorService shoeColorService;

    @Mock
    private  ShoesRepository shoesRepository;

    @Mock
    private RedirectAttributes redirectAttributes;

    @Mock
    private Model model;

    @Captor
    private ArgumentCaptor<Shoe> captorShoe;

    @Captor
    private ArgumentCaptor<List<Type>> captorTypeList;

    @Captor
    private ArgumentCaptor<List<Price>> captorPriceList;

    @Captor
    private ArgumentCaptor<List<Size>> captorSizeList;

    @Captor
    private ArgumentCaptor<List<Brand>> captorBrandList;

    @Captor
    private ArgumentCaptor<List<Sex>> captorSexList;

    @Captor
    private ArgumentCaptor<List<Color>> captorColorList;

    private static List<Shoe> mockShoeList;

    private static List<Type> mockTypeList;

    private static List<Price> mockPriceList;

    private static List<Size> mockSizeList;

    private static List<Brand> mockBrandList;

    private static List<Sex> mockSexList;

    private static List<Color> mockColorList;

    private static Shoe mockShoe;

    @BeforeAll
    static void setUp() {
        mockShoeList = new ArrayList<>();
        mockTypeList = new ArrayList<>();
        mockPriceList = new ArrayList<>();
        mockSizeList = new ArrayList<>();
        mockBrandList = new ArrayList<>();
        mockSexList = new ArrayList<>();
        mockColorList = new ArrayList<>();

        Type mockType = new Type();
        mockType.setIdType(1L);
        mockType.setNameType("TestType");
        mockTypeList.add(mockType);

        Price mockPrice = new Price();
        mockPrice.setIdPrice(1L);
        mockPrice.setAmountPrice("TestPrice");
        mockPriceList.add(mockPrice);

        Size mockSize = new Size();
        mockSize.setIdSize(1L);
        mockSize.setNoSize("TestSize");
        mockSizeList.add(mockSize);

        Brand mockBrand = new Brand();
        mockBrand.setIdBrand(1L);
        mockBrand.setNameBrand("TestNameBrand");
        mockBrand.setCountryBrand("TestCountryBrand");
        mockBrandList.add(mockBrand);

        Sex mockSex = new Sex();
        mockSex.setIdSex(1L);
        mockSex.setNameSex("TestSex");
        mockSexList.add(mockSex);

        Color mockColor = new Color();
        mockColor.setIdColor(1L);
        mockColor.setNameColor("TestColor");
        mockColorList.add(mockColor);

        mockShoe = new Shoe();
        mockShoe.setIdShoe(1L);
        mockShoe.setNameShoe("TestName");
        mockShoe.setAliasShoe("TestAlias");
        mockShoe.setTypeShoe(mockType);
        mockShoe.setPriceShoe(mockPrice);
        mockShoe.setSizeShoe(mockSizeList);
        mockShoe.setBrandShoe(mockBrand);
        mockShoe.setSexShoe(mockSexList);
        mockShoe.setColorShoe(mockColorList);

        mockShoeList.add(mockShoe);
    }

    @Test
    void showForm() throws Exception {
        when(shoesRepository.findAll()).thenReturn(mockShoeList);

        when(shoeTypeService.getAllTypes()).thenReturn(mockTypeList);

        when(shoePriceService.getAllPrices()).thenReturn(mockPriceList);

        when(shoeSizeService.getAllSizes()).thenReturn(mockSizeList);

        when(shoeBrandService.getAllBrands()).thenReturn(mockBrandList);

        when(shoeSexService.getAllSexes()).thenReturn(mockSexList);

        when(shoeColorService.getAllColors()).thenReturn(mockColorList);

        String viewName = addShoeController.showForm(model);
        assertEquals("add",viewName);

        verify(model, times(1))
                .addAttribute(eq("typeList"), captorTypeList.capture());
        List<Type> listTypeArgument = captorTypeList.getValue();
        assertEquals(mockTypeList.size(), listTypeArgument.size());

        verify(model, times(1))
                .addAttribute(eq("priceList"), captorPriceList.capture());
        List<Price> listPriceArgument = captorPriceList.getValue();
        assertEquals(mockPriceList.size(), listPriceArgument.size());

        verify(model, times(1))
                .addAttribute(eq("sizeList"), captorSizeList.capture());
        List<Size> listSizeArgument = captorSizeList.getValue();
        assertEquals(0, listSizeArgument.size());

        verify(model, times(1))
                .addAttribute(eq("brandList"), captorBrandList.capture());
        List<Brand> listBrandArgument = captorBrandList.getValue();
        assertEquals(mockBrandList.size(), listBrandArgument.size());

        verify(model, times(1))
                .addAttribute(eq("sexList"), captorSexList.capture());
        List<Sex> listSexArgument = captorSexList.getValue();
        assertEquals(mockSexList.size(), listSexArgument.size());

        verify(model, times(1))
                .addAttribute(eq("colorList"), captorColorList.capture());
        List<Color> listColorArgument = captorColorList.getValue();
        assertEquals(mockColorList.size(), listColorArgument.size());

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(addShoeController).build();
        mockMvc.perform(
                get("/addShoe"))
                .andExpect(status().isOk())
                .andExpect(view().name("add"));
    }

    @Test
    void saveShoe() throws Exception {
        Shoe mockShoeSaved = new Shoe();
        BeanUtils.copyProperties(mockShoe, mockShoeSaved);
        mockShoeSaved.setNameShoe("SecondName");

        String viewName = addShoeController.saveShoe(mockShoe, redirectAttributes);
        assertEquals("redirect:/shoes",viewName);

        Gson gson = new GsonBuilder().create();
        String jsonFromMockShoe = gson.toJson(mockShoe);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(addShoeController).build();
        mockMvc.perform(
                post("/addShoe/saveShoe").content(jsonFromMockShoe)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(redirectedUrl("/shoes?pageNumber=0"));
    }
}